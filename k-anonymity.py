"""
# Data Privacy
## Trabalho 1
- Leonardo Sampaio
- Lucas S. Fernandes
- Lucas Primo Muraro

*** Recomenda-se a leitura do notebook Jupyter de nome k-anonymity.ipynb
"""

import pandas as pd
from datetime import datetime

dataset = pd.read_csv("epidemias_2.csv")
dataset

def prepararDados(dataset):
    datasetAnon = dataset.copy()
    datasetAnon['ano_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).year
    datasetAnon['mes_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).month
    datasetAnon['dia_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).day
    del datasetAnon['nome']
    del datasetAnon['data_nascimento']
    return datasetAnon
datasetAnon = prepararDados(dataset)
datasetAnon

def converterDataset(dataset):
    datasetAnon = prepararDados(dataset)
    datasetAnon.sort_values(by=['genero','estado','cidade','ano_nascimento','mes_nascimento','dia_nascimento'], inplace = True);
    return datasetAnon
datasetAnon = converterDataset(dataset)
datasetAnon

def separarClasses(dataset, k):
    datasetAnon = converterDataset(dataset)
    r = k
    r_old = 0
    r_new = r

    total_rows = len(datasetAnon)

    classes = []
    while (r_new <= total_rows):
        x = datasetAnon[r_old:r_new].copy()
        classes.append(x)
        aux = r_old
        r_old = r_new 
        r_new = r_new + r

        if(r_new > total_rows):
            x = datasetAnon[aux:].copy()
            classes[len(classes)-1] = x
    return classes

classes = separarClasses(dataset, 8) #k=8 como exemplo
for i in range(0,len(classes)):
    print (classes[i])
    print ('\n')


def checkEqual(df, size):
    teste = []
    for i in range(0,size):       
        for j in range(0,size):
            if(i != j):
                if(df.iloc[i]['dia_nascimento'] != df.iloc[j]['dia_nascimento']):
                    teste = [False, 'dia_nascimento']
                    return teste
                else:
                    if(df.iloc[i]['mes_nascimento'] != df.iloc[j]['mes_nascimento']):
                        teste = [False, 'mes_nascimento']
                        return teste
                    else:
                        if(df.iloc[i]['ano_nascimento'] != df.iloc[j]['ano_nascimento']):
                            teste = [False, 'ano_nascimento']
                            return teste
                        else:
                                if(df.iloc[i]['cidade'] != df.iloc[j]['cidade']):
                                    teste = [False, 'cidade']
                                    return teste
                                else:
                                    if(df.iloc[i]['estado'] != df.iloc[j]['estado']):
                                        teste = [False, 'estado']
                                        return teste
                                    else:
                                        if(df.iloc[i]['genero'] != df.iloc[j]['genero']):
                                            teste = [False, 'genero']
                                            return teste
                                        else:
                                            teste = [True]
    return teste

def generalizar(df, attr, size):
    df.loc[:, attr] = '*'
    if(attr == 'dia_nascimento'):
        df.loc[:, attr] = '**'
    if(attr == 'mes_nascimento'):
        df.loc[:, attr] = '**'
        df.loc[:, 'dia_nascimento'] = '**'
    if(attr == 'ano_nascimento'):
        df.loc[:, attr] = '**'
        df.loc[:, 'dia_nascimento'] = '**'
        df.loc[:, 'mes_nascimento'] = '**'
        
        
def anonimizar(dataset, k):
    classes = separarClasses(dataset, k)
    classes_total_rows = len(classes)
    for i in range(0,classes_total_rows):
        size = len(classes[i])

        aux = checkEqual(classes[i], size)
        while( not aux[0] ):
            generalizar(classes[i], aux[1], size)
            aux = checkEqual(classes[i], len(classes[i]))
    return classes       
classes = anonimizar(dataset, 8) #k=8 como exemplo
for i in range(0, len(classes)):
    print (classes[i])
    print('\n')
    
    
def dataset_Anonimizado(dataset, k):
    classes = anonimizar(dataset, k);
    df = []
    for r in classes:
        for x in r.values.tolist():
            df.append(x)
    headers = ['genero','cidade','estado','doencax','ano_nascimento','mes_nascimento','dia_nascimento']        
    datasetAnon = pd.DataFrame(df, columns=headers)
    datasetAnon['data_nascimento'] = datasetAnon[['mes_nascimento','dia_nascimento', 'ano_nascimento']].apply(lambda x: '{}/{}/{}'.format(x[0],x[1],x[2]), axis=1) 
    del(datasetAnon['ano_nascimento'])
    del(datasetAnon['mes_nascimento'])
    del(datasetAnon['dia_nascimento'])
    datasetAnon = datasetAnon[['genero','data_nascimento','cidade','estado','doencax']].copy()
    return datasetAnon
novoDataset = dataset_Anonimizado(dataset, 8) # fazendo k=8




def k_anonymity(dataset, k):
    return dataset_Anonimizado(dataset, k)
    
k = 2
datasetAnon_2 = k_anonymity(dataset, k);
datasetAnon_2.to_csv("epidemias_2_Anon_2.csv")

k = 4
datasetAnon_4 = k_anonymity(dataset, k);
datasetAnon_4.to_csv("epidemias_2_Anon_4.csv")

k = 8
datasetAnon_8 = k_anonymity(dataset, k);
datasetAnon_8.to_csv("epidemias_2_Anon_8.csv")

k = 16
datasetAnon_16 = k_anonymity(dataset, k);
datasetAnon_16.to_csv("epidemias_2_Anon_16.csv")




def precisao(dataset):
    Na = 4
    soma = 0
    D = len(dataset)
    for x in dataset:
        for i in range(0, D):
            h = 0
            HGV = 1
            element = dataset.iloc[i][x] 
            if (x == 'genero'):
                if (element == '*'):
                    h = 1
            elif (x == 'cidade'):
                if (element == '*'):
                    h = 1
            elif (x == 'estado'):
                if (element == '*'):
                    h = 1
            elif (x == 'data_nascimento'):
                HGV = 3
                if(element == '**/**/**'):
                    h = 3
                elif ('**/**' in element):
                    h = 2
                elif ('**/' in element):
                    h = 1
            soma = soma + (h/HGV)
    return 1 - soma/(D*Na)

print("A precisão para k = 2 é de {}".format(precisao(datasetAnon_2)))
print("A precisão para k = 4 é de {}".format(precisao(datasetAnon_4)))
print("A precisão para k = 8 é de {}".format(precisao(datasetAnon_8)))
print("A precisão para k = 16 é de {}".format(precisao(datasetAnon_16)))
